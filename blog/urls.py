from django.conf.urls import url
from .views import *



urlpatterns = [
    url(r'^$', blog_list, name="blog_list"),
    url(r'^blog_add/', blog_add, name="blog_add"),
    url(r'^blog_comment/', blog_comment, name="blog_comment"),

    url(r'^(?P<slug>[\w-]+)/$', blog_details, name="blog_details"),
    # url(r'^(?P<slug>[\w-]+)/blog_comment/', blog_comment, name="blog_comment"),
]
