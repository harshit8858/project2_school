from django.db import models
from django.db.models.signals import pre_save
from django.utils.text import slugify
from django.core.urlresolvers import reverse


class Blog(models.Model):
    subject = models.CharField(max_length=1000, unique=True)
    slug = models.SlugField(unique=True)
    timestamp = models.DateTimeField(auto_now=True)
    last_updated = models.DateTimeField(auto_now_add=True)
    content = models.TextField(max_length=10000)
    author = models.CharField(max_length=1000)

    def __str__(self):
        return self.subject

    def get_absolute_url(self):
        return reverse("blog:blog_details", kwargs={"slug": self.slug})

    class Meta:
        ordering = ["-timestamp", "-last_updated"]


def create_slug(instance, new_slug=None):
    slug = slugify(instance.subject)

    if new_slug is not None:
        slug = new_slug
    qs = Blog.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()

    if exists:
        new_slug = "%s-%s" % (slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug

    # signal receiver
def pre_save_blog_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)

pre_save.connect(pre_save_blog_receiver, sender=Blog)


class Comment(models.Model):
    blog = models.ForeignKey(Blog)
    comment = models.CharField(max_length=1000)
    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.comment
