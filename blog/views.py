from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from .forms import *


def blog_list(request):
    blog = Blog.objects.all()
    context = {
        'blog': blog,
    }
    return render(request, 'blog/blog_list.html', context)


def blog_add(request):
    if request.method == 'POST':
        form = BlogForm(request.POST)
        if form.is_valid():
            f = form.save()
            f.save()
            # return redirect('blog:blog_list')
            return HttpResponseRedirect(f.get_absolute_url())
    else:
        form = BlogForm()
    context = {
        'form': form,
    }
    return render(request, 'blog/blog_add.html', context)


def blog_details(request, slug):
    instance = get_object_or_404(Blog, slug=slug)
    comment = Comment.objects.all()
    context = {
        'instance': instance,
        'comment': comment,
    }
    return render(request, 'blog/blog_details.html', context)


def blog_comment(request):
    # instance = get_object_or_404(Blog, slug=slug)
    blog_id = request.POST["take_id"]
    blog_obj = Blog.objects.get(id=blog_id)  # for grabbing particular post's comments
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            f = form.save(commit=False)
            f.blog = blog_obj
            f.save()
            print("hi")
            # return redirect('blog:blog_list')
            return HttpResponseRedirect(blog_obj.get_absolute_url())
    else:
        form = CommentForm()
    context = {
        'form': form,
    }
    return render(request, 'blog/blog_details', context)
